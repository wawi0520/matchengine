"""
This module implements a match engine that, according to the score method, matches
buyers for each seller.

Example:
	The command line argument must be either --help, --all, or --seller_id id
		$ python match_engine.py --help
		$ python match_engine.py --all
		$ python match_engine.py --seller_id 1640fb77-135c-43d4-8196-835e07245683
"""

import json
import sys
import getopt

class MatchEngine:
	""" The MatchEngine class implements a match engine that finds buyers for each seller
	"""
	def __init__(self, data):
		""" Initialize the information of buyers and sellers, from the given input data (json)

		The structure of buyers and sellers will be a dictionary that maps the id to a dictionary of
			1. a SET of geography ids, and
			2. a SET of industry ids
		The set structure will help implement the scoring method.

		Suppose the following input data is given:
			{"type": "seller", "details": {"geography_ids": [101, 103, 107, 114, 116, 118], "industry_ids": [251, 253]}, "id": "6372c203-f540-4208-8def-94edfc8257e8"}
			{"type": "seller", "details": {"geography_ids": [106, 108, 110, 112, 115, 116, 117, 123, 124], "industry_ids": [251, 268]}, "id": "1640fb77-135c-43d4-8196-835e07245683"}
		Then the sellers data structure will be:
			{'6372c203-f540-4208-8def-94edfc8257e8': {'geography_ids': {101, 103, 107, 114, 116, 118}, 'industry_ids': {251, 253}}}
			{'1640fb77-135c-43d4-8196-835e07245683': {'geography_ids': {106, 108, 110, 112, 115, 116, 117, 123, 124}, 'industry_ids': {251, 268}}}

		Args:
			data: the input data in json format
		"""
		self.buyers = dict()
		self.sellers = dict()
		for d in data:
			key = d["id"]
			if "buyer" == d["type"]:
				# Get the geography_ids and industry_ids from a buyer
				info = self.buyers.get(key)
				if info == None:
					# The buyer information does not exist in our seller structure; create a new one
					info = {"geography_ids": set(d["details"]["geography_ids"]), "industry_ids":set(d["details"]["industry_ids"])}
					self.buyers.update({key:info})
				else:
					# The information with the same id is already there. So we update the informatin instead
					# of creating a new entry.
					self.buyers[key]["geography_ids"].update(set(d["details"]["geography_ids"]))
					self.buyers[key]["industry_ids"].update(set(d["details"]["industry_ids"]))
			elif "seller" == d["type"]:
				# Get the geography_ids and industry_ids from a seller
				info = self.sellers.get(key)
				if info == None:
					# The seller information does not exist in our seller structure; create a new one
					info = {"geography_ids": set(d["details"]["geography_ids"]), "industry_ids":set(d["details"]["industry_ids"])}
					self.sellers.update({key:info})
				else:
					# The information with the same id is already there. So we update the informatin instead
					# of creating a new entry.
					self.sellers[key]["geography_ids"].update(set(d["details"]["geography_ids"]))
					self.sellers[key]["industry_ids"].update(set(d["details"]["industry_ids"]))
			else:
				# Unknown type. Report error.
				print("Corrupted data: type must be eithor buyer or seller")
				sys.exit(2)

	def match_one(self, seller_id):
		""" Match buyers for a seller

		This function will match buyers for a seller. A match means the seller and buyer have
		at least one intersecting industry and at least one intersecting geography. If there
		exists multiple buyers, the following scoring method is used:
			score = (the number of intersecting industry) + (the number of intersecting geography)
		Buyers will be ordered in such a way that a buyer with higher score precedes those with
		lower score.

		Args:
			seller_id: the seller id

		Returns:
			None if the given seller_id is invalid, or
			tuple: A tuple that contains a seller and its potential buyers
		"""
		seller = self.sellers.get(seller_id)
		if seller == None:
			# Unable to find the seller according to the seller id
			print("Unknown seller id")
			return None

		candidates = []
		seller_info = self.sellers[seller_id]
		for buyer_id in self.buyers:
			buyer_info = self.buyers[buyer_id]
			# Calculate the number of intersecting industry
			geography_score = len((buyer_info["geography_ids"] & seller_info["geography_ids"]))
			# Calculate the number of intersecting geography
			industry_score = len((buyer_info["industry_ids"] & seller_info["industry_ids"]) )
			# A match means the seller and buyer have at least one intersecting industry AND
			# at least one intersecting geography.
			if geography_score != 0 and industry_score != 0:
				# We only process the match pair.
				candidates.append((buyer_id, geography_score + industry_score))

		# Sort the results by the scores
		candidates = sorted(candidates,key=lambda x:x[1],reverse=True)

		seller_buyers_pair = []
		for candidate in candidates:
			seller_buyers_pair.append(candidate[0])
		return (seller_id, seller_buyers_pair) # return a tuple (seller_id, list of buyer_id)

	def match_all(self):
		""" Match buyers for all sellers

		This function will iterate each seller and internally call match_one to find
		buyers for each seller

		Returns:
			list: a list of tuples that contains a seller and its potential buyers
		"""
		buyers_list = []
		for seller_id in self.sellers:
			# Match buyers for a seller
			seller_buyers_tuple = self.match_one(seller_id)
			# Only add a valid result into the list
			if seller_buyers_tuple != None:
				buyers_list.append(seller_buyers_tuple)
		return buyers_list

	def get_buyers(self):
		return self.buyers

	def get_sellers(self):
		return self.sellers


if __name__ == "__main__":
	# Load data for the input file
	with open('sample_data.json') as data_file:
		data = json.load(data_file)

	# Create a MatchEngine object, given the data
	match_engine = MatchEngine(data)

	argv = sys.argv[1:] # Stripe the first argument, which is the filename of this source code
	try:
		# Only support --help, --all, and --seller_id id.
		opts, args = getopt.getopt(argv,"",["help","all","seller_id="])
		if not opts:
			print ('usage: match_engine.py [--all|--seller_id id]')
			sys.exit(2)
	except getopt.GetoptError:
		print ('usage: match_engine.py [--all|--seller_id id]')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("--all"):
			# Match buyers for all sellers
			buyers_list = match_engine.match_all()
			# Output the result
			for (seller, buyers) in buyers_list:
				print("seller:", seller)
				for buyer in buyers:
					print("\tbuyer:", buyer)
		elif opt in ("--seller_id"):
			# Match buyers for a seller identified by the seller id
			buyers_tuple = match_engine.match_one(arg) # arg is the seller id
			if buyers_tuple != None:
				# Output the result
				print("seller:", buyers_tuple[0])
				for buyer in buyers_tuple[1]:
					print("\tbuyer:", buyer)
		elif opt in ("--help"):
			# Show the usage
			print ('usage: match_engine.py [--all|--seller_id id]')
